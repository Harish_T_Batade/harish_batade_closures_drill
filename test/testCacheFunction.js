
const cacheFunction = require('../cacheFunction')

let callback = cacheFunction(cb)

function cb(val){
    console.log("cb invoked which converts passed arguments to string and return it as result")
return val.toString()
}


console.log(typeof cacheFunction(cb))// returning func
console.log("------------------")
//console.log(callback(1,2,6.9,"uguh",6789))

let call1=callback(1,2,6.9,"uguh",6789)

console.log("logging1 "+call1)

let call2 = callback(1,2,6.9,"uguh",6789)

console.log("logging2 "+call2)

let call3 = callback(1,2,6.9,"uguh",6789)

console.log("logging3 "+call3)



//test for given situation in which callback function called thrice but it invoked it once at the first call
if(call1==null && !compare(call2,call3))
{
    console.log("Nice job done")
}
else
{
    console.log("sorry! your code is broken")
}


function compare(result1FromTheCbInvocation,result2FromTheCbInvocation)
{
    
    let length=0;
    let situation = false;
    if(result1FromTheCbInvocation.length!=result2FromTheCbInvocation.length)
    {
        situation = true;
    }
    else
    {
        length=Math.max(result1FromTheCbInvocation.length,result2FromTheCbInvocation.length)

        for (let index = 0; index < length; index++) {
            if(result1FromTheCbInvocation[index]!=result2FromTheCbInvocation[index])
            {
                situation = true;
                break;
            }

            
        }

    }
    return situation;
}