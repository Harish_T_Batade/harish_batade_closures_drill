
// Return an object that has two methods called `increment` and `decrement`.
    // `increment` should increment a counter variable in closure scope and return it.
    // `decrement` should decrement the counter variable and return it.
    
let makeCounter = function () {
    var Counter = 0;
    function change(val) {
      Counter += val;
      return Counter;
    }
    return {
      increment: function () {
        return change(1);
      },
  
      decrement: function () {
        return change(-1);
      },
    };
  };
  
  module.exports = makeCounter;