// Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned

function limitFunctionCallCount(cb, n) {
    
    let count=0;
    return function callBack()
{
    count++;
    
    if(count<=n)
    {
        console.log("callBack invoked,count= "+count)
        cb();
    }
    else{
        return null;
    }
    
}

}

module.exports = limitFunctionCallCount;