// Should return a function that invokes `cb`.
    // A cache (object) should be kept in closure scope.
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `cb` again.
    // `cb` should only ever be invoked once for a given set of arguments.

    
function cacheFunction(cb) {
    
    let givenArguments= [];
    let results=[];
    let cache = {memory : givenArguments, result : results};  //object cache to store given argument and results for that argument separately  

    return function invokingFunc(...value) // returning function
{
    let presentArg=value;
    let invokedresult=0;
    let matched =[];

    let lengthOfMemory=0

    // to converting object to a string for further comparison
    for (let index = 0; index < value.length; index++) {
        const element = value[index];
        let status= typeof element;
        let capture="";
        if(status=="object")
        {
            capture=JSON.stringify(element)
            value.splice(index,1,capture)
        }
        
    }
    cache.memory.push(value)
    
    console.log("invoking func excicuting")

    if(cache.memory.length>1)
    {
        lengthOfMemory=cache.memory.length-1
    }
    else
    {
        lengthOfMemory=cache.memory.length
    }
    
    for (let index = 0; index < lengthOfMemory; index++) {    
        matched.push(compare(cache.memory[index],presentArg))
    }

    let countFalse = matched.filter(function filtering(element,index)
    {
        if(element==false)
        {
            return true;
        }
        else
        {
            return false;
        }
    });
           //below first condition for the starting case
    if((cache.memory.length==1 && cache.result==0) || countFalse.length==0)//condition to check weather there was match for the arguments
    {
        invokedresult = cb(presentArg);
        cache.result.push(invokedresult)
        return null;
    }
    else if(countFalse.length>=1)
    {
        return cache.result[matched.indexOf(false)];
    }
}
function compare(elementInCache,presentArgument) // function to convert arguments into string and compare character by character
{
    let elementInCacheString=elementInCache.toString();
    let presentArgumentString=presentArgument.toString();
    let length=0;
    let situation = false; // variable to say if arguments is matched or not
    if(elementInCacheString.length!=presentArgumentString.length)
    {
        situation = true;
    }
    else
    {
        length=Math.max(elementInCacheString.length,presentArgumentString.length)

        for (let index = 0; index < length; index++) {
            if(elementInCacheString[index]!=presentArgumentString[index])
            {
                situation = true;
                break;
            }

            
        }

    }
    return situation;
}
}

module.exports = cacheFunction;
